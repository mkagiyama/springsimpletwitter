package jp.co.simpletwitter.form;

import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotBlank;

public class LoginForm {
    private String name;
    private String account;
    @NotBlank(message ="必須項目です")
    @Pattern(regexp="[a-zA-Z0-9]*",message="パスワードは英数である必要があります。")

    private String password;
    private String email;
    @NotBlank(message ="必須項目です")
    private String accountOrEmail;

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getAccountOrEmail() {
		return accountOrEmail;
	}
	public void setAccountOrEmail(String accountOrEmail) {
		this.accountOrEmail = accountOrEmail;
	}
}
