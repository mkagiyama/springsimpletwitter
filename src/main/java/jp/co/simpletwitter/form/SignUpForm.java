package jp.co.simpletwitter.form;

import org.hibernate.validator.constraints.NotBlank;

public class SignUpForm {
	@NotBlank(message = "必須項目です")
	String account;
	String name;
	String email;
	@NotBlank(message = "必須項目です")
	String password;
	String description;

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
}
