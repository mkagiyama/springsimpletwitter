package jp.co.simpletwitter.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import jp.co.simpletwitter.entity.UserMessageEntity;

public interface UserMessageMapper {
	List<UserMessageEntity> getUserMessages(@Param("num")int num);

}
