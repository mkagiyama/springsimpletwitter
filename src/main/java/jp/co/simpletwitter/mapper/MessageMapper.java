package jp.co.simpletwitter.mapper;

import jp.co.simpletwitter.entity.MessageEntity;

public interface MessageMapper {
	void messageRegister(MessageEntity entity);
}
