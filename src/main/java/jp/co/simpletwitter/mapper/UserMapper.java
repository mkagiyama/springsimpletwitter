package jp.co.simpletwitter.mapper;

import org.apache.ibatis.annotations.Param;

import jp.co.simpletwitter.entity.UserEntity;

public interface UserMapper {
	UserEntity getLoginUser(@Param("account")String account, @Param("password")String password);

	void signUp(UserEntity entity);
}
