package jp.co.simpletwitter.viewmodel;

public class SignUpView {
	public String getRegisterMessage() {
		return "登録完了しました";
	}
	public String getErrorMessage() {
		return "以下の内容を確認してください";
	}
	public String getDuplicateMessage() {
		return "社員番号が重複しています";
	}
	public String getIllegalArgumentMessage() {
		return "登録できませんでした";
	}

}
