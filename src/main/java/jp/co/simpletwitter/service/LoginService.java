package jp.co.simpletwitter.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.simpletwitter.entity.UserEntity;
import jp.co.simpletwitter.form.LoginForm;
import jp.co.simpletwitter.mapper.UserMapper;

@Service
public class LoginService {
	@Autowired
	private UserMapper userMapper;

	public String userLoginBlank() {
		return "以下の内容を確認してください。";
	}

	public String userLogin(LoginForm form) {
		try {
			getLoginUser(form.getAccountOrEmail() ,form.getPassword());
			return "OK";
		} catch (IllegalArgumentException e) {
			return "認証に失敗しました。";
		} catch(Exception e) {
			return "認証に失敗しました。";
		}
	}

	public UserEntity getLoginUser(String account, String password) {
		UserEntity entity = userMapper.getLoginUser(account, password);
		if(entity == null) {
			throw new IllegalStateException();
		}
		return entity;
	}

}
