package jp.co.simpletwitter.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.simpletwitter.entity.MessageEntity;
import jp.co.simpletwitter.entity.UserMessageEntity;
import jp.co.simpletwitter.form.MessageForm;
import jp.co.simpletwitter.form.UserMessageForm;
import jp.co.simpletwitter.mapper.MessageMapper;
import jp.co.simpletwitter.mapper.UserMessageMapper;

@Service
public class MessageService {

	private static final int LIMIT_NUM = 1000;

	@Autowired
	private MessageMapper messageMapper;

	@Autowired
	private UserMessageMapper userMessageMapper;

	public String userMessageBlank() {
		return "以下の内容を確認してください";
	}

    public String register(MessageForm form, String userId) {
    	try {
    		form.setUserId(Integer.parseInt(userId));
    		messageMapper.messageRegister(convertEntity(form));
    		return "ツイートしました";
    	} catch(IllegalArgumentException e) {
    		return "ツイートできませんでした";
    	} catch(Exception e) {
    		return "ツイートできませんでした";
    	}
    }

	public MessageEntity convertEntity(MessageForm form) {
		MessageEntity entity = new MessageEntity();
		BeanUtils.copyProperties(form, entity);
		return entity;
	}

	public List<UserMessageForm> getUserMessage() {
		List<UserMessageEntity> userMessages = userMessageMapper.getUserMessages(LIMIT_NUM);
		List<UserMessageForm> resultList = convertUserMessageForm(userMessages);
		return resultList;
	}

	public List<UserMessageForm> convertUserMessageForm(List<UserMessageEntity> userMessages) {
		List<UserMessageForm> resultList = new ArrayList<UserMessageForm>();
		for(UserMessageEntity entity : userMessages) {
			UserMessageForm messages = new UserMessageForm();
			BeanUtils.copyProperties(entity, messages);
			resultList.add(messages);
		}
		return resultList;
	}
}
