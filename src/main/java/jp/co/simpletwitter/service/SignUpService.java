package jp.co.simpletwitter.service;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;

import jp.co.simpletwitter.entity.UserEntity;
import jp.co.simpletwitter.form.SignUpForm;
import jp.co.simpletwitter.mapper.UserMapper;

@Service
public class SignUpService {
	@Autowired
	private UserMapper userMapper;

	public String userSignUpBlank() {
		return "以下の内容を確認してください" ;
	}

	public String userSignUp(SignUpForm form) {
		try {
			userMapper.signUp(convertEntity(form));
			return "登録完了しました";
		} catch(IllegalArgumentException e) {
			return "登録できませんでした";
		} catch(DuplicateKeyException e) {
			return "アカウント名が重複しています";
		} catch(Exception e) {
			return "登録できませんでした";
		}
	}

	public UserEntity convertEntity(SignUpForm form) {
		UserEntity entity = new UserEntity();
		BeanUtils.copyProperties(form, entity);
		return entity;
	}
}
