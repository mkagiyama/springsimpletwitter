package jp.co.simpletwitter.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.simpletwitter.entity.UserEntity;
import jp.co.simpletwitter.form.MessageForm;
import jp.co.simpletwitter.form.UserMessageForm;
import jp.co.simpletwitter.service.MessageService;

@Controller
public class TopController {
	@Autowired
	private MessageService messageService;

	@RequestMapping(value = "/top", method = RequestMethod.GET)

	public String top(HttpServletRequest request, @ModelAttribute("messageForm")MessageForm form, Model model) {
		HttpSession session = request.getSession();
		UserEntity loginUser = (UserEntity) session.getAttribute("loginUser");
		boolean isShowMessageForm;
        if (loginUser != null) {
            isShowMessageForm = true;
        } else {
            isShowMessageForm = false;
        }

        List<UserMessageForm> messages = messageService.getUserMessage();

        model.addAttribute("messages", messages);
        model.addAttribute("messageForm", form);
        model.addAttribute("isShowMessageForm", isShowMessageForm);
		return "top";
	}
}
