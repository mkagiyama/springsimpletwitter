package jp.co.simpletwitter.controller;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.simpletwitter.entity.UserEntity;
import jp.co.simpletwitter.form.LoginForm;
import jp.co.simpletwitter.service.LoginService;

@Controller
public class LoginController {
	@Autowired
	private HttpSession session;
	@Autowired
	private LoginService loginService;

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String login(Model model) {
		LoginForm loginForm = new LoginForm();
		model.addAttribute("loginForm", loginForm);
		return "login";
	}

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String getLoginnfo(@Valid @ModelAttribute("loginForm")LoginForm form, BindingResult result, Model model) {
		if (result.hasErrors()) {
			String message = loginService.userLoginBlank();
			model.addAttribute("loginForm", form);
			model.addAttribute("message", message);
		} else {
			String message = loginService.userLogin(form);
			if(message.equals("OK")) {
				UserEntity user = loginService.getLoginUser(form.getAccountOrEmail(), form.getPassword());
				session.setAttribute("loginUser", user);
				return "redirect:/top";
			}
			model.addAttribute("loginForm", form);
			model.addAttribute("message", message);
		}
		return "login";
	}
}
