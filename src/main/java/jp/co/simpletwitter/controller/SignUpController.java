package jp.co.simpletwitter.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.simpletwitter.form.SignUpForm;
import jp.co.simpletwitter.service.SignUpService;

@Controller
public class SignUpController {
	@Autowired
	private SignUpService signUpService;

	@RequestMapping(value = "/signup", method = RequestMethod.GET)
	public String signup(Model model) {
		SignUpForm signUpForm = new SignUpForm();
		model.addAttribute("signUpForm", signUpForm);
		return "signup";
	}

	@RequestMapping(value = "/signup", method = RequestMethod.POST)
	public String userSignUp(@Valid @ModelAttribute SignUpForm form, BindingResult result, Model model) {
		if (result.hasErrors()) {
			String message = signUpService.userSignUpBlank();
			model.addAttribute("signUpForm", form);
			model.addAttribute("message", message);
		} else {
			String message = signUpService.userSignUp(form);
			model.addAttribute("message", message);
		}
		return "signup";
	}
}