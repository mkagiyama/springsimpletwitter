package jp.co.simpletwitter.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.simpletwitter.entity.UserEntity;
import jp.co.simpletwitter.form.MessageForm;
import jp.co.simpletwitter.service.MessageService;

@Controller
public class MessageController {

	@Autowired

	private MessageService messageService;

	@RequestMapping(value = "/newMessage", method = RequestMethod.POST)
	public String message(HttpServletRequest request, @Valid @ModelAttribute("messageForm")MessageForm form, BindingResult result, Model model) {
		 if(result.hasErrors()) {
			 String infoMessages = messageService.userMessageBlank();
			 model.addAttribute("infoMessages" , infoMessages);
			 return "redirect:/top";

		 } else {
			 HttpSession session = request.getSession();
			 UserEntity loginUser = (UserEntity)session.getAttribute("loginUser");
			 String infoMessages = messageService.register(form, loginUser.getId());
			 model.addAttribute("infoMessages" , infoMessages);
			 return "redirect:/top";
		 }

	}

}
