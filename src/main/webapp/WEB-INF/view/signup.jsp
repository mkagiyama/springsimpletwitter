<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link href="<c:url value="/resources/css/style.css"/>" 	rel="stylesheet">
    <title>ユーザー登録</title>
    </head>
    <body>
        <p class="error">${message}</p>
        <div class="main-contents">
			<form:form modelAttribute="signUpForm">
				名前<form:input path="name"/>（名前はあなたの公開プロフィールに表示されます）<br />
				ログインID<form:input path="account"/><form:errors path="account" class="errors" /><br />
				パスワード<form:input path="password" type="password" /><form:errors path="password" class="errors" /><br />
				メールアドレス<form:input path="email"/><br />
				説明<form:textarea path="description" class="description" /><br />
				<input type="submit" value="登録" /><br />
				<a href="./">戻る</a>
			</form:form>
        </div>
    </body>
</html>