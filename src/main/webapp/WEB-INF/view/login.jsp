<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>ログイン</title>
		<link href="<c:url value="/resources/css/style.css"/>" 	rel="stylesheet">
	</head>

	<body>
    	<p class="error">${message}</p>
		<div class="main-contents">
			<form:form modelAttribute="loginForm">
				ログインID or メールアドレス<form:input path="accountOrEmail"/><form:errors path="accountOrEmail" class="errors" /><br />
				パスワード<form:input path="password" type="password"/><form:errors path="password" class="errors"/><br />
				<input type="submit" value="ログイン" /> <br />
				<a href="./">戻る</a>
	    	</form:form>
     	</div>
	</body>
</html>